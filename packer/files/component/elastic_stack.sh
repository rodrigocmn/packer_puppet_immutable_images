#!/bin/sh -x
echo **** Downloading required Puppet modules ****
/opt/puppetlabs/bin/puppet module install elastic-elasticsearch --version 6.1.0 --target-dir /tmp/packer-puppet-masterless/module-0
/opt/puppetlabs/bin/puppet module install puppetlabs-java --version 2.3.0 --target-dir /tmp/packer-puppet-masterless/module-0
/opt/puppetlabs/bin/puppet module install elastic-kibana --version 6.0.0 --target-dir /tmp/packer-puppet-masterless/module-0
/opt/puppetlabs/bin/puppet module install elastic-logstash --version 6.0.2 --target-dir /tmp/packer-puppet-masterless/module-0

# Copy Logstash configuration uploaded by Packer to the app configuration folder.
# By using this approach we keep the main Packer template file reusable by other components.
echo **** Copying Logstash configuration ****
sudo mkdir -p /etc/logstash/conf.d
sudo cp -r /tmp/packer-puppet-masterless/config/* /etc/logstash/conf.d