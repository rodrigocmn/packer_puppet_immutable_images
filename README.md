# Immutable Image

This is an example to demonstrate how to create immutable images on MS Azure using Packer and Puppet tools. We will use Elastic Stack as the component example.

## Pre-requisites

Obvious ones:

- Puppet and Packer installed
- Azure subscription and permissions to deploy resources (you can use the free trial)

Not so obvious ones:

- Azure Resource Group where the images are going to be created.


## Packer commands
The packer commands below must be executed under the packer folder in this project.

### Base Image
For this demo we will install Puppet in order to use puppet manifests in our component specific images and cloud-init to enable bootstraping the component image instantiation.

```bash
packer build -var-file=vars/base/base_el74.json templates/base_template.json
```
We can use the debug flag (-debug) if needed.

### Component Image
The following command uses the base image created in the previous section to create a new component image (Elastic Stack in this case).

```bash
packer build -var-file=vars/component/elastic_stack.json templates/component_template.json
```