include ::java

class { 'elasticsearch':
  manage_repo   => true,
  repo_version  => '6.x',
  version       => '6.1.1',
  instances     => {
    'es-01' => {
      'config' => {
        'network.bind_host' => '0.0.0.0',
        'node.master' => true,
        'node.data' => true,
        'transport.host'=> localhost,
        'transport.tcp.port' => 9300,
        'node.name'    => '${HOSTNAME}',
        'discovery.zen.ping.unicast.hosts' => [
          'localhost:9300'
        ]
      }
    }
  },
}

service { 'elasticsearch-es-01':
  ensure  => stopped,
  enable  => true,
  stop    => "service elasticsearch-es-01 stop",
  require => Class['elasticsearch']
}

file {'remove_directory':
  ensure  => absent,
  path    => '/var/lib/elasticsearch/es-01/0',
  recurse => true,
  purge   => true,
  force   => true,
  require => Service['elasticsearch-es-01']
}

class {'logstash':
  version => '6.1.1'
}

# You must provide a valid pipeline configuration for the service to start.
logstash::configfile { 'logstash.conf':
  source => '/etc/logstash/conf.d/logstash.conf',
}

# Logstash plugins
logstash::plugin { 'logstash-input-beats': }

class { 'kibana':
  ensure => '6.1.1',
  config => {
    'server.port' => '8080',
    'server.host' => '0.0.0.0',
    'elasticsearch.url' => 'http://localhost:9200'
  }
}
